package com.atlassian.graphql.router;

import graphql.ExecutionResultImpl;
import graphql.GraphQLError;
import graphql.language.SourceLocation;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class GraphQLRouterErrorFactoryTest {

    @Test()
    public void testNoErors() {
        Map mapResult = new HashMap();

        List<GraphQLError> gQLErrors = GraphQLRouterErrorFactory.getErrors(mapResult);
        assertEquals(0, gQLErrors.size());
    }

    @Test
    public void testExecutionResult() {
        final String errorMsg = "error message";

        List<GraphQLError> errors = Collections.singletonList(
                new GraphQLRouterErrorFactory.BasicGraphQLError(errorMsg, null));
        List<GraphQLError> gQLErrors = GraphQLRouterErrorFactory.getErrors(new ExecutionResultImpl(errors));

        assertEquals(1, gQLErrors.size());
        GraphQLRouterErrorFactory.BasicGraphQLError graphQLError = (GraphQLRouterErrorFactory.BasicGraphQLError) gQLErrors.get(0);
        assertEquals(errorMsg, graphQLError.getMessage());
        assertNull(graphQLError.getLocations());
    }

    private Map buildError(String errorMsg, int errorLine, int errorColumn, Integer errorLine2, Integer errorColumn2) {
        Map location = new HashMap();
        location.put("line", errorLine);
        location.put("column", errorColumn);

        List locations = new ArrayList();
        locations.add(location);

        if (errorLine2 != null && errorColumn2 != null) {
            Map location2 = new HashMap();
            location2.put("line", errorLine2);
            location2.put("column", errorColumn2);

            locations.add(location2);
        }

        Map error = new HashMap();
        error.put("message", errorMsg);
        error.put("locations", locations);

        return error;
    }

    @Test
    public void testSingleError() throws Exception {
        final int errorLine = 1;
        final int erroColumn = 2;
        final String errorMsg = "error message";

        List errors = new ArrayList();
        errors.add(buildError(errorMsg, errorLine, erroColumn, null, null));

        Map mapResult = new HashMap();
        mapResult.put("errors", errors);

        List<GraphQLError> gQLErrors = GraphQLRouterErrorFactory.getErrors(mapResult);

        assertEquals(1, gQLErrors.size());
        GraphQLRouterErrorFactory.BasicGraphQLError graphQLError = (GraphQLRouterErrorFactory.BasicGraphQLError) gQLErrors.get(0);
        assertEquals(errorMsg, graphQLError.getMessage());
        List<SourceLocation> gqleLocations = graphQLError.getLocations();
        assertEquals(1, gqleLocations.size());
        SourceLocation gqleLoc = gqleLocations.get(0);
        assertEquals(errorLine, gqleLoc.getLine());
        assertEquals(erroColumn, gqleLoc.getColumn());
    }

    @Test
    public void testMultipleError() throws Exception {
        List errors = new ArrayList();

        final int errorLine1 = 1;
        final int erroColumn1 = 2;
        final String errorMsg1 = "an error message";

        errors.add(buildError(errorMsg1, errorLine1, erroColumn1, null, null));

        final int errorLine2 = 3;
        final int erroColumn2 = 4;
        final String errorMsg2 = "another error message";
        final int errorLine3 = 5;
        final int erroColumn3 = 6;

        errors.add(buildError(errorMsg2, errorLine2, erroColumn2, errorLine3, erroColumn3));

        Map mapResult = new HashMap();
        mapResult.put("errors", errors);

        List<GraphQLError> gQLErrors = GraphQLRouterErrorFactory.getErrors(mapResult);

        assertEquals(2, gQLErrors.size());

        GraphQLRouterErrorFactory.BasicGraphQLError graphQLError = (GraphQLRouterErrorFactory.BasicGraphQLError) gQLErrors.get(0);
        assertEquals(errorMsg1, graphQLError.getMessage());
        List<SourceLocation> gqleLocations = graphQLError.getLocations();
        assertEquals(1, gqleLocations.size());
        SourceLocation gqleLoc = gqleLocations.get(0);
        assertEquals(errorLine1, gqleLoc.getLine());
        assertEquals(erroColumn1, gqleLoc.getColumn());

        graphQLError = (GraphQLRouterErrorFactory.BasicGraphQLError) gQLErrors.get(1);
        assertEquals(errorMsg2, graphQLError.getMessage());
        gqleLocations = graphQLError.getLocations();
        assertEquals(2, gqleLocations.size());
        gqleLoc = gqleLocations.get(0);
        assertEquals(errorLine2, gqleLoc.getLine());
        assertEquals(erroColumn2, gqleLoc.getColumn());
        gqleLoc = gqleLocations.get(1);
        assertEquals(errorLine3, gqleLoc.getLine());
        assertEquals(erroColumn3, gqleLoc.getColumn());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidArg() {
        GraphQLRouterErrorFactory.getErrors(new Object());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidErrorList() {
        Map mapResult = new HashMap();
        mapResult.put("errors", new Object());

        GraphQLRouterErrorFactory.getErrors(mapResult);
    }

    private Map setupErrors(Object errorList) {
        Map mapResult = new HashMap();
        mapResult.put("errors", errorList);

        return mapResult;
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidList() throws Exception {
        GraphQLRouterErrorFactory.getErrors(setupErrors(new Object()));
    }

    @Test
    public void testEmptyErrorList() {
        GraphQLRouterErrorFactory.getErrors(setupErrors(null));
    }

    private Map setupError(Object message, Object locations) {
        Map error = new HashMap();
        error.put("message", message);
        error.put("locations", locations);

        List errors = new ArrayList();
        errors.add(error);

        return setupErrors(errors);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidListEntry()
    {
       List errors = new ArrayList();
       errors.add(new Object());

       GraphQLRouterErrorFactory.getErrors(setupErrors(errors));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidMessage() throws Exception {
        GraphQLRouterErrorFactory.getErrors(setupError(new Object(), null));
    }

    @Test()
    public void testNullMessage() throws Exception {
        List<GraphQLError> gQLErrors = GraphQLRouterErrorFactory.getErrors(setupError(null, null));

        assertEquals(1, gQLErrors.size());
        GraphQLRouterErrorFactory.BasicGraphQLError graphQLError = (GraphQLRouterErrorFactory.BasicGraphQLError) gQLErrors.get(0);
        assertEquals("", graphQLError.getMessage());
        List<SourceLocation> gqleLocations = graphQLError.getLocations();
        assertEquals(0, gqleLocations.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidLocations() throws Exception {
        GraphQLRouterErrorFactory.getErrors(setupError("error message", new Object()));
    }

    @Test()
    public void testNullLocations() throws Exception {
        final String errorMsg = "an error message for null locations";
        List<GraphQLError> gQLErrors = GraphQLRouterErrorFactory.getErrors(setupError(errorMsg, null));

        assertEquals(1, gQLErrors.size());
        GraphQLRouterErrorFactory.BasicGraphQLError graphQLError = (GraphQLRouterErrorFactory.BasicGraphQLError) gQLErrors.get(0);
        assertEquals(errorMsg, graphQLError.getMessage());
        List<SourceLocation> gqleLocations = graphQLError.getLocations();
        assertEquals(0, gqleLocations.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidLocationLine() throws Exception {
        Map location = new HashMap();
        location.put("line", null);
        List locations = singletonList(location);

        GraphQLRouterErrorFactory.getErrors(setupError("error message", locations));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidLocationColumn() throws Exception {
        Map location = new HashMap();
        location.put("line", 1);
        location.put("column", "junk");
        List locations = singletonList(location);

        GraphQLRouterErrorFactory.getErrors(setupError("error message", locations));
    }

}
