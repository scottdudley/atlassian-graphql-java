package com.atlassian.graphql.json.jersey;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.reflect.TypeToken;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;

import static com.atlassian.graphql.json.jersey.JerseyResourceMethodDataFetcher.DEFAULT_ARGUMENT_CONVERTERS;
import static java.util.Objects.requireNonNull;
import static org.junit.Assert.assertEquals;

public class JerseyResourceMethodDataFetcherTest {

    private JerseyResourceMethodDataFetcher dataFetcher;

    @Before
    public void setUp() throws NoSuchMethodException {
        dataFetcher = new JerseyResourceMethodDataFetcher(
                this.getClass().getMethod("setUp"),
                "a-field-name",
                this,
                null,
                Collections.emptyList(),
                Collections.emptyMap(),
                null,
                DEFAULT_ARGUMENT_CONVERTERS);
    }

    @Test
    public void testConvertFieldArgumentValue_String() {
        assertEquals("str", dataFetcher.convertArgumentValue(null, "str", String.class));
    }

    @Test
    public void testConvertFieldArgumentValue_Integer() {
        assertEquals(12, dataFetcher.convertArgumentValue(null, "12", Integer.class));
        assertEquals(12, dataFetcher.convertArgumentValue(null, 12, Integer.class));
        assertEquals(12, dataFetcher.convertArgumentValue(null, "12", int.class));
        assertEquals(12, dataFetcher.convertArgumentValue(null, 12, int.class));
    }

    @Test
    public void testConvertFieldArgumentValue_IntegerToShort() {
        assertEquals((short) 12, dataFetcher.convertArgumentValue(null, "12", Short.class));
        assertEquals((short) 12, dataFetcher.convertArgumentValue(null, 12, Short.class));
        assertEquals((short) 12, dataFetcher.convertArgumentValue(null, "12", short.class));
        assertEquals((short) 12, dataFetcher.convertArgumentValue(null, 12, short.class));
    }

    @Test
    public void testConvertFieldArgumentValue_ShortToInteger() {
        assertEquals(12, dataFetcher.convertArgumentValue(null, (short) 12, Integer.class));
        assertEquals(12, dataFetcher.convertArgumentValue(null, (short) 12, int.class));
    }

    @Test
    public void testConvertFieldArgumentValue_ShortToShort() {
        assertEquals((short) 12, dataFetcher.convertArgumentValue(null, (short) 12, Short.class));
        assertEquals((short) 12, dataFetcher.convertArgumentValue(null, (short) 12, short.class));
    }

    @Test
    public void testConvertFieldArgumentValue_Boolean() {
        assertEquals(true, dataFetcher.convertArgumentValue(null, "true", Boolean.class));
        assertEquals(true, dataFetcher.convertArgumentValue(null, "true", boolean.class));
        assertEquals(true, dataFetcher.convertArgumentValue(null, true, Boolean.class));
        assertEquals(true, dataFetcher.convertArgumentValue(null, true, boolean.class));
    }

    @Test
    public void testConvertFieldArgumentValue_String_To_Enum() {
        assertEquals(TestEnum.Value2, dataFetcher.convertArgumentValue(null, "Value2", TestEnum.class));
    }

    @Test
    public void testConvertFieldArgumentValue_Enum_To_Enum() {
        assertEquals(TestEnum.Value2, dataFetcher.convertArgumentValue(null, TestEnum.Value2, TestEnum.class));
    }

    @Test
    public void testConvertFieldArgumentValue_List() {
        assertEquals(
                Lists.newArrayList("a", "b"),
                dataFetcher.convertArgumentValue(
                        null,
                        Lists.newArrayList("a", "b"),
                        new TypeToken<List<String>>(){}.getType()));
        assertEquals(
                Lists.newArrayList(ContentId.valueOf("1"), ContentId.valueOf("2")),
                dataFetcher.convertArgumentValue(
                        null,
                        Lists.newArrayList(ContentId.valueOf("1"), ContentId.valueOf("2")),
                        new TypeToken<List<ContentId>>(){}.getType()));
    }

    @Test
    public void testConvertFieldArgumentValue_Set() {
        assertEquals(
                Sets.newHashSet("a", "b"),
                dataFetcher.convertArgumentValue(
                        null,
                        Lists.newArrayList("a", "b"),
                        new TypeToken<Set<String>>(){}.getType()));
        assertEquals(
                Sets.newHashSet(ContentId.valueOf("1"), ContentId.valueOf("2")),
                dataFetcher.convertArgumentValue(
                        null,
                        Lists.newArrayList(ContentId.valueOf("1"), ContentId.valueOf("2")),
                        new TypeToken<Set<ContentId>>(){}.getType()));
    }

    @Test
    public void testConvertFieldArgumentValue_SortedSet() {
        assertEquals(
                Sets.newTreeSet(Lists.newArrayList("a", "b")),
                dataFetcher.convertArgumentValue(
                        null,
                        Lists.newArrayList("a", "b"),
                        new TypeToken<SortedSet<String>>(){}.getType()));
        assertEquals(
                Sets.newTreeSet(Lists.newArrayList(ContentId.valueOf("1"), ContentId.valueOf("2"))),
                dataFetcher.convertArgumentValue(
                        null,
                        Lists.newArrayList(ContentId.valueOf("1"), ContentId.valueOf("2")),
                        new TypeToken<SortedSet<ContentId>>(){}.getType()));
    }

    public static class ContentId implements Comparable {
        private final String value;

        public ContentId(final String value) {
            this.value = requireNonNull(value);
        }

        public static ContentId valueOf(final String value) {
            return new ContentId(value);
        }

        @Override
        public boolean equals(final Object obj) {
            return value.equals(((ContentId) obj).value);
        }

        @Override
        public int hashCode() {
            return value.hashCode();
        }

        @Override
        public String toString() {
            return value;
        }

        @Override
        public int compareTo(final Object obj) {
            return value.compareTo(((ContentId) obj).value);
        }
    }

    /*
     * An enum with values that have anonymous implementations, as this changes how we compare enums
     */
    public enum TestEnum {
        Value1() {}, Value2() {}
    }
}
