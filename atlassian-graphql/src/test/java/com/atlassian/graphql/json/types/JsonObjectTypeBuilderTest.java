package com.atlassian.graphql.json.types;

import com.atlassian.graphql.GraphQLTestTypeBuilderImpl;
import com.atlassian.graphql.test.json.GraphQLJsonSerializationTestHelper;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;

public class JsonObjectTypeBuilderTest {
    private final GraphQLJsonSerializationTestHelper testHelper =
            new GraphQLJsonSerializationTestHelper(new GraphQLTestTypeBuilderImpl());

    @Test
    public void testSerializeSubTypes() throws Exception {
        final Subclass1 obj = new Subclass1("baseField", "subclassFieldValue");
        testHelper.assertGraphAndJsonSerializeSame(BaseType.class, obj);
        assertEquals(
                "{\n" +
                "  \"type\" : \"subclass1\",\n" +
                "  \"value\" : \"baseField\",\n" +
                "  \"subclass1Field\" : \"subclassFieldValue\"\n" +
                "}",
                testHelper.serializeGraphToJson(Subclass1.class, obj, false, null));
    }

    @Test
    public void testSerializeSubTypes_FromMap() throws Exception {
        final Map obj = ImmutableMap.of(
                "type", "subclass1",
                "value", "baseField",
                "subclass1Field", "subclassFieldValue");
        assertEquals(
                "{\n" +
                "  \"type\" : \"subclass1\",\n" +
                "  \"value\" : \"baseField\",\n" +
                "  \"subclass1Field\" : \"subclassFieldValue\"\n" +
                "}",
                testHelper.serializeGraphToJson(Subclass1.class, obj, false, null));
    }

    @Test
    public void testJsonPropertyMethod() throws Exception {
        final ObjectWithJsonMethod obj = new ObjectWithJsonMethod();
        assertEquals(
                "{\n" +
                "  \"field\" : \"fieldValue\"\n" +
                "}",
                testHelper.serializeGraphToJson(ObjectWithJsonMethod.class, obj, false, null));
    }

    @JsonTypeInfo(
            use = JsonTypeInfo.Id.NAME,
            include = JsonTypeInfo.As.PROPERTY,
            property = "type"
    )
    @JsonSubTypes({
            @JsonSubTypes.Type(name = "subclass1", value = Subclass1.class),
            @JsonSubTypes.Type(name = "subclass2", value = Subclass2.class)
    })
    public static class BaseType {
        @JsonProperty
        private String value;

        public BaseType(final String value) {
            this.value = value;
        }
    }

    public static class Subclass1 extends BaseType {
        @JsonProperty
        private String subclass1Field;

        public Subclass1(final String baseField, final String subclass1Field) {
            super(baseField);
            this.subclass1Field = subclass1Field;
        }
    }

    public static class Subclass2 extends BaseType {
        @JsonProperty
        private String subclass2Field;

        public Subclass2(final String baseField, final String subclass2Field) {
            super(baseField);
            this.subclass2Field = subclass2Field;
        }
    }

    public static class ObjectWithJsonMethod {
        @JsonProperty
        public String getField() {
            return "fieldValue";
        }

        // to be ignored
        @JsonProperty
        public void setField(String value) {
        }
    }
}
