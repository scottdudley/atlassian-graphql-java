package com.atlassian.graphql.types;

import com.atlassian.graphql.GraphQLTestTypeBuilderImpl;
import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.test.json.GraphQLJsonSerializer;
import com.atlassian.graphql.test.json.GraphQLJsonSerializerOptions;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import graphql.Scalars;
import graphql.schema.GraphQLFieldDefinition;
import org.junit.Test;

import java.lang.reflect.Type;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class DynamicTypeBuilderTest {
    private final GraphQLJsonSerializer serializer = new GraphQLJsonSerializer(
            new ObjectMapper(),
            EnumSet.noneOf(GraphQLJsonSerializerOptions.class));

    @Test
    public void testSerialize() throws Exception {
        final DynamicType type = new DynamicType("dynamicType", ImmutableMap.of(
                "str", String.class,
                "number", Integer.class));
        final Map map = ImmutableMap.of(
                "str", "value",
                "number", 99);
        assertEquals(
                "{\n" +
                "  \"str\" : \"value\",\n" +
                "  \"number\" : 99\n" +
                "}",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(), type, map));
    }

    @Test
    public void testSerializeNestedObject() throws Exception {
        final DynamicType nestedType = new DynamicType("nestedDynamicType", ImmutableMap.of(
                "nestedStr", String.class));
        final DynamicType type = new DynamicType("dynamicType", ImmutableMap.of(
                "str", String.class,
                "nested", nestedType));

        final Map nestedMap = ImmutableMap.of("nestedStr", "nested");
        final Map map = ImmutableMap.of(
                "str", "value",
                "nested", nestedMap);
        assertEquals(
                "{\n" +
                "  \"str\" : \"value\",\n" +
                "  \"nested\" : {\n" +
                "    \"nestedStr\" : \"nested\"\n" +
                "  }\n" +
                "}",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(), type, map));
    }

    @Test
    public void testExtensionContributedField() throws Exception {
        final DynamicType type = new DynamicType("dynamicType", ImmutableMap.of(
                "field", String.class));
        final Map map = ImmutableMap.of("field", "value");

        final GraphQLExtensions extensions = new GraphQLExtensions() {
            @Override
            public void contributeFields(
                    final String typeName,
                    final Type type,
                    final List<GraphQLFieldDefinition> fields,
                    final GraphQLTypeBuilderContext context) {

                fields.add(GraphQLFieldDefinition.newFieldDefinition()
                           .name("extendedField")
                           .type(Scalars.GraphQLString)
                           .dataFetcher(env -> "extendedFieldValue")
                           .build());
            }
        };
        assertEquals(
                "{\n" +
                "  \"extendedField\" : \"extendedFieldValue\",\n" +
                "  \"field\" : \"value\"\n" +
                "}",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(extensions), type, map));
    }
}
