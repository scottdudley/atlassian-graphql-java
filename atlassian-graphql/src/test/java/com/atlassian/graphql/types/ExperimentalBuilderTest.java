package com.atlassian.graphql.types;

import com.atlassian.graphql.GraphQLTestTypeBuilderImpl;
import com.atlassian.graphql.annotations.GraphQLExperimental;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.schema.GraphQLTypeSchemaPrinter;
import com.atlassian.graphql.test.json.GraphQLJsonSerializer;
import com.atlassian.graphql.test.json.GraphQLJsonSerializerOptions;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLSchema;
import org.junit.Test;

import java.util.EnumSet;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ExperimentalBuilderTest {
    private final GraphQLJsonSerializer serializer = new GraphQLJsonSerializer(
            new ObjectMapper(),
            EnumSet.of(GraphQLJsonSerializerOptions.ADD_ROOT_FIELD));

    @Test
    public void testSerialize() throws Exception {
        final ObjectWithExperimentalFields obj = new ObjectWithExperimentalFields();
        GraphQLTestTypeBuilderImpl x = new GraphQLTestTypeBuilderImpl();
        assertEquals(
                "{\n" +
                "  \"experimentalField\" : \"str\",\n" +
                "  \"listField\" : [ \"item\" ],\n" +
                "  \"selfField\" : {\n" +
                "    \"experimentalField\" : \"str\",\n" +
                "    \"listField\" : [ \"item\" ],\n" +
                "    \"regularField\" : \"reg\"\n" +
                "  },\n" +
                "  \"regularField\" : \"reg\"\n" +
                "}",
                serializer.serializeUsingGraphQL(x, ObjectWithExperimentalFields.class, obj));
    }

    @Test
    public void testExperimentalType() {
        final RootGraphQLTypeBuilder typeBuilder = new RootGraphQLTypeBuilder();
        final GraphQLObjectType type = (GraphQLObjectType) typeBuilder.buildType(ObjectWithExperimentalFields.class);
        final GraphQLSchema schema = GraphQLSchema.newSchema().query(type).build();
        assertEquals("type ObjectWithExperimentalFields {\n" +
                "  # @Experimental field: experimentalField\n" +
                "  experimentalField: String\n" +
                "  # @Experimental field: listField\n" +
                "  listField: [String]\n" +
                "  # @Experimental field: selfField\n" +
                "  selfField: ObjectWithExperimentalFields\n" +
                "  regularField: String\n" +
                "}",
                new GraphQLTypeSchemaPrinter(GraphQLTypeSchemaPrinter.Style.Flat).print(schema.getQueryType()));
    }

    public static class ObjectWithExperimentalFields {
        public ObjectWithExperimentalFields() {
            this("str", Lists.newArrayList("item"), "reg");
        }

        public ObjectWithExperimentalFields(final String experimentalField, final List<String> listField,
                                            final String regularField) {
            this.experimentalField = experimentalField;
            this.listField = listField;
            this.selfField = this;
            this.regularField = regularField;
        }

        @GraphQLName
        @GraphQLExperimental
        private String experimentalField;

        @GraphQLName
        @GraphQLExperimental
        private List<String> listField;

        @GraphQLName
        @GraphQLExperimental
        private ObjectWithExperimentalFields selfField;

        @GraphQLName
        private String regularField;


    }
}
