package com.atlassian.graphql.types;

import com.atlassian.graphql.GraphQLTestTypeBuilderImpl;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.annotations.GraphQLNonNull;
import com.atlassian.graphql.schema.GraphQLTypeSchemaPrinter;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.test.json.GraphQLJsonSerializer;
import com.atlassian.graphql.test.json.GraphQLJsonSerializerOptions;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLSchema;
import graphql.schema.GraphQLType;
import org.junit.Test;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Type;
import java.util.EnumSet;
import java.util.List;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class NonNullTypeBuilderTest {
    private final GraphQLJsonSerializer serializer = new GraphQLJsonSerializer(
            new ObjectMapper(),
            EnumSet.of(GraphQLJsonSerializerOptions.ADD_ROOT_FIELD));

    @Test
    public void testSerialize() throws Exception {
        final ObjectWithNotNullFields obj = new ObjectWithNotNullFields();
        assertEquals(
                "{\n" +
                        "  \"stringField\" : \"str\",\n" +
                        "  \"listField\" : [ \"item\" ],\n" +
                        "  \"selfField\" : {\n" +
                        "    \"stringField\" : \"str\",\n" +
                        "    \"listField\" : [ \"item\" ]\n" +
                        "  }\n" +
                        "}",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(), ObjectWithNotNullFields.class, obj));
    }

    @Test
    public void testNonNullType() {
        final RootGraphQLTypeBuilder typeBuilder = new RootGraphQLTypeBuilder();
        final GraphQLObjectType type = (GraphQLObjectType) typeBuilder.buildType(ObjectWithNotNullFields.class);
        final GraphQLSchema schema = GraphQLSchema.newSchema().query(type).build();
        assertEquals(
                "type ObjectWithNotNullFields {\n" +
                        "  stringField: String!\n" +
                        "  listField: [String]!\n" +
                        "  selfField: ObjectWithNotNullFields!\n" +
                        "}",
                new GraphQLTypeSchemaPrinter(GraphQLTypeSchemaPrinter.Style.Flat).print(schema.getQueryType()));
    }

    @Test
    public void testNonNullTypeBuilderDoesNotApplyOnAlreadyNonNullType() {
        GraphQLTypeBuilder mockTypeBuilder = mock(GraphQLTypeBuilder.class);
        GraphQLType mockGraphQLType = mock(GraphQLType.class);

        NonNullTypeBuilder nonNullTypeBuilder = new NonNullTypeBuilder(mockTypeBuilder);
        when(mockTypeBuilder.buildType(anyString(), any(), any(), any()))
                .thenAnswer(x -> new graphql.schema.GraphQLNonNull(mockGraphQLType));

        Type mockType = mock(Type.class);
        AnnotatedElement mockAnnotatedElement = mock(AnnotatedElement.class);
        GraphQLTypeBuilderContext mockGraphQLTypeBuilderContext = mock(GraphQLTypeBuilderContext.class);

        GraphQLType type = nonNullTypeBuilder.buildType("testTypeName", mockType, mockAnnotatedElement, mockGraphQLTypeBuilderContext);

        assertThat(type, is(instanceOf(graphql.schema.GraphQLNonNull.class)));
        assertThat(((graphql.schema.GraphQLNonNull) type).getWrappedType(), is(not(instanceOf(graphql.schema.GraphQLNonNull.class))));

        verify(mockTypeBuilder).buildType("testTypeName", mockType, mockAnnotatedElement, mockGraphQLTypeBuilderContext);
    }

    public static class ObjectWithNotNullFields {
        public ObjectWithNotNullFields() {
            this("str", Lists.newArrayList("item"));
        }

        public ObjectWithNotNullFields(final String stringField, final List<String> listField) {
            this.stringField = stringField;
            this.listField = listField;
            this.selfField = this;
        }

        @GraphQLName
        @GraphQLNonNull
        private String stringField;

        @GraphQLName
        @GraphQLNonNull
        private List<String> listField;

        @GraphQLName
        @GraphQLNonNull
        private ObjectWithNotNullFields selfField;
    }
}
