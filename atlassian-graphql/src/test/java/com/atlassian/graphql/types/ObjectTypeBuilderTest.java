package com.atlassian.graphql.types;

import com.atlassian.graphql.GraphQLTestTypeBuilderImpl;
import com.atlassian.graphql.annotations.GraphQLDeprecate;
import com.atlassian.graphql.annotations.GraphQLIgnore;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.annotations.GraphQLNonNull;
import com.atlassian.graphql.schema.GraphQLTypeSchemaPrinter;
import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.test.json.GraphQLJsonSerializer;
import com.atlassian.graphql.test.json.GraphQLJsonSerializerOptions;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;
import graphql.GraphQLException;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLSchema;
import org.codehaus.jackson.annotate.JsonProperty;
import org.junit.Test;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Type;
import java.util.EnumSet;
import java.util.List;
import java.util.function.Function;

import static graphql.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ObjectTypeBuilderTest {
    private final GraphQLJsonSerializer serializer = new GraphQLJsonSerializer(
            new ObjectMapper(),
            EnumSet.noneOf(GraphQLJsonSerializerOptions.class));

    @Test
    public void testSerialize() throws Exception {
        final Subclass obj = new Subclass("baseFieldValue", "subclassFieldValue");
        assertEquals(
                "{\n" +
                        "  \"baseField\" : \"baseFieldValue\",\n" +
                        "  \"subclassField\" : \"subclassFieldValue\"\n" +
                        "}",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(), Subclass.class, obj));
    }

    @Test
    public void testSerializeFieldWithBothMethodAndField() throws Exception {
        final ClassWithMethodAndField obj = new ClassWithMethodAndField();
        assertEquals(
                "Must serialize getter method over field value",
                "{\n" +
                "  \"field\" : \"methodValue\"\n" +
                "}",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(), ClassWithMethodAndField.class, obj));
    }

    @Test
    public void testBuildObjectWithMethodFields() throws Exception {
        final ClassWithOperationFields obj = new ClassWithOperationFields();
        assertEquals(
                "{\n" +
                "  \"field\" : \"fieldValue\",\n" +
                "  \"methodField\" : \"getterValue\",\n" +
                "  \"methodFieldBool\" : true,\n" +
                "  \"methodFieldGet\" : \"methodValue\",\n" +
                "  \"methodFieldName\" : \"namedValue\"\n" +
                "}",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(), ClassWithOperationFields.class, obj));
    }

    @Test
    public void testValueTransformer() throws Exception {
        // GraphQLTypeBuilder.getValueTransformer() will add '_typebuilder'
        final GraphQLTypeBuilder typeBuilderWithValueTransformer = new GraphQLTypeBuilderDelegator() {
            @Override
            public boolean canBuildType(final Type type, final AnnotatedElement element) {
                return type == String.class;
            }

            @Override
            public Function<Object, Object> getValueTransformer(final Type type, final AnnotatedElement element) {
                return obj -> obj + "_typebuilder";
            }

            @Override
            protected GraphQLTypeBuilder getTypeBuilder(Type type, AnnotatedElement element) {
                return new ScalarTypeTypeBuilder();
            }
        };

        // GraphQLExtensions.getValueTransformer() will add '_extensions'
        final GraphQLExtensions extensions = new GraphQLExtensions() {
            @Override
            public List<GraphQLTypeBuilder> getAdditionalTypeBuilders(
                    final GraphQLTypeBuilder typeBuilder,
                    final GraphQLExtensions extensions) {

                return Lists.newArrayList(typeBuilderWithValueTransformer);
            }

            @Override
            public Function<Object, Object> getValueTransformer(final Type type, final AnnotatedElement element) {
                return obj -> obj + "_extensions";
            }
        };

        final GraphQLJsonSerializer serializer = new GraphQLJsonSerializer(
                new ObjectMapper(),
                EnumSet.of(GraphQLJsonSerializerOptions.ADD_ROOT_FIELD));
        assertEquals(
                "Both GraphQLExtensions and GraphQLTypeBuilder must transform value in the field DataFetcher",
                "\"value_extensions_typebuilder\"",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(extensions), String.class, "value"));
    }

    @Test
    public void testFieldWithGenericTypeInSuperclass() throws Exception {
        final ClassWithGenericTypeField<String> obj = new ClassWithGenericTypeField<>(Lists.newArrayList("value"));
        final GraphQLJsonSerializer serializer = new GraphQLJsonSerializer(
                new ObjectMapper(),
                EnumSet.of(GraphQLJsonSerializerOptions.ADD_ROOT_FIELD));
        assertEquals(
                "{\n" +
                "  \"field\" : {\n" +
                "    \"list\" : [ \"value\" ]\n" +
                "  }\n" +
                "}",
                serializer.serializeUsingGraphQL(new GraphQLTestTypeBuilderImpl(), new TypeToken<ClassWithGenericTypeField<String>>(){}.getType(), obj));
    }

    @Test
    public void testFieldWithArgument() {
        GraphQLObjectType type = (GraphQLObjectType) new RootGraphQLTypeBuilder().buildType(ClassWithMethodAndArguments.class);
        GraphQLFieldDefinition field = type.getFieldDefinition("methodFieldArgument");
        assertNotNull(field);

        assertEquals("blah", field.getArgument("str").getDefaultValue());
        assertEquals(5, field.getArgument("int").getDefaultValue());
        assertEquals(5, field.getArgument("intObj").getDefaultValue());
        assertEquals(3.14f, field.getArgument("float").getDefaultValue());
        assertEquals(3.14, field.getArgument("double").getDefaultValue());
        assertEquals(true, field.getArgument("bool").getDefaultValue());
        assertEquals(true, field.getArgument("boolObj").getDefaultValue());
    }

    @Test
    public void testFieldWithNonNullArgument() {
        GraphQLObjectType type = (GraphQLObjectType) new RootGraphQLTypeBuilder().buildType(ClassWithNotNullArgument.class);
        GraphQLFieldDefinition field = type.getFieldDefinition("argumentField");
        assertNotNull(field);

        assertTrue(field.getArgument("id").getType() instanceof graphql.schema.GraphQLNonNull);
    }

    @Test
    public void testFieldNonNullArgumentSerialized() {
        final RootGraphQLTypeBuilder typeBuilder = new RootGraphQLTypeBuilder();
        final GraphQLObjectType type = (GraphQLObjectType) typeBuilder.buildType(ClassWithNotNullArgument.class);
        final GraphQLSchema schema = GraphQLSchema.newSchema().query(type).build();
        assertEquals(
                "type ClassWithNotNullArgument {\n" +
                        "  argumentField(id: String!): String!\n" +
                        "}",
                new GraphQLTypeSchemaPrinter(GraphQLTypeSchemaPrinter.Style.Flat).print(schema.getQueryType()).trim());
    }

    @Test(expected = GraphQLException.class)
    public void testClassWithFieldTypeThatHasNoFields() throws Exception {
        new RootGraphQLTypeBuilder().buildType(ClassWithFieldTypeThatHasNoFields.class);
    }
    public static class ClassWithMethodAndArguments {
        @GraphQLName
        public String methodFieldArgument(
                @GraphQLName("str") @DefaultValue("blah") String strArg,
                @GraphQLName("int") @DefaultValue("5") int intArg,
                @GraphQLName("intObj") @DefaultValue("5") Integer intObjArg,
                @GraphQLName("float") @DefaultValue("3.14") float floatArg,
                @GraphQLName("double") @DefaultValue("3.14") double doubleArg,
                @GraphQLName("bool") @DefaultValue("true") boolean boolArg,
                @GraphQLName("boolObj") @DefaultValue("true") Boolean boolObjArg
        ) {
            return "argumentValue";
        }
    }

    @Test
    public void testFieldWithDeprecatedFields() {
        GraphQLObjectType type = (GraphQLObjectType) new RootGraphQLTypeBuilder().buildType(ClassWithDeprecatedFields.class);
        GraphQLFieldDefinition field = type.getFieldDefinition("deprecatedField");
        assertNotNull(field);

        assertEquals("Don't use me", field.getDeprecationReason());

        GraphQLFieldDefinition fieldViaMethod = type.getFieldDefinition("deprecatedFieldViaMethod");
        assertNotNull(fieldViaMethod);

        assertEquals("Don't use me either", fieldViaMethod.getDeprecationReason());
    }

    public static class ClassWithFieldTypeThatHasNoFields {

        @GraphQLName
        private NoFields field;
    }
    public static class NoFields {

        private String notAGraphQLField;
    }
    public static class ClassWithGenericTypeField<T> {

        @GraphQLName
        public Pagination<T> field;

        public ClassWithGenericTypeField(final List<T> value) {
            field = new Pagination<>(value);
        }
    }
    public static class PaginationBase<T> {

        @GraphQLName
        public List<T> list;
    }
    public static class Pagination<T> extends PaginationBase<T> {

        public Pagination(final List<T> list) {
            this.list = list;
        }
    }
    public static class BaseType {


        @GraphQLName
        private String baseField;
        public BaseType(final String baseField) {
            this.baseField = baseField;
        }
        @GraphQLName
        public String ignored() {
            return "ignored";
        }

    }
    public static interface InterfaceType {
        @GraphQLIgnore
        String ignored();

    }
    public static class Subclass extends BaseType implements InterfaceType {


        @GraphQLName
        private String subclassField;

        public Subclass(final String baseField, final String subclassField) {
            super(baseField);
            this.subclassField = subclassField;
        }

        @GraphQLName
        public String ignored() {
            return super.ignored();
        }
    }
    public static class ClassWithOperationFields {


        @GraphQLName
        private String field = "fieldValue";

        @GraphQLName("methodFieldName")
        public String namedMethodField() {
            return "namedValue";
        }

        @GraphQLName
        public String methodFieldGet() {
            return "methodValue";
        }
        @GraphQLName
        public String getMethodField() {
            return "getterValue";
        }

        @GraphQLName
        public boolean isMethodFieldBool() {
            return true;
        }

    }
    public static class ClassWithMethodAndField {

        @JsonProperty
        private String field = "fieldValue";
        @JsonProperty
        public String field() {
            return "methodValue";
        }

    }
    @Retention(RetentionPolicy.RUNTIME)
    public @interface DefaultValue {
        String value();

    }
    private static class ClassWithNotNullArgument {

        @GraphQLName
        @GraphQLNonNull
        private String getArgumentField(@GraphQLName("id") @GraphQLNonNull String id) {
            return "hi-" + id;
        }

    }

    public static class ClassWithDeprecatedFields {

        @GraphQLDeprecate("Don't use me")
        @GraphQLName
        private String deprecatedField = "deprecated";


        private String deprecatedFieldViaMethod = "deprecatedViaMethod";

        @GraphQLDeprecate("Don't use me either")
        @GraphQLName
        public String getDeprecatedFieldViaMethod() {
            return deprecatedFieldViaMethod;
        }
    }
}
