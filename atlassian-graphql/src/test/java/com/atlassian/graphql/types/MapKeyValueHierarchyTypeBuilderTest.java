package com.atlassian.graphql.types;

import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.json.types.JsonRootGraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.google.common.collect.ImmutableMap;
import graphql.execution.ExecutionContext;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.DataFetchingEnvironmentImpl;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLSchema;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;


public class MapKeyValueHierarchyTypeBuilderTest {


    @Test
    public void testSerialize() throws Exception {
        final Map<String, Object> map = ImmutableMap.of("key", ImmutableMap.of("nestedKey", "nestedValue"));
        final List<Map<String, Object>> keyValuePairList = serializeMapToGraphQL(map);
        assertEquals(1, keyValuePairList.size());

        final Map<String, Object> keyValuePair = keyValuePairList.get(0);
        assertEquals("key", keyValuePair.get("key"));
        final Map<String, Object> nestedMap = (Map<String, Object>) keyValuePair.get("fields");
        assertEquals("nestedValue", nestedMap.get("nestedKey"));
    }

    @SuppressWarnings("unchecked")
    private List<Map<String, Object>> serializeMapToGraphQL(Map<String, Object> map) throws Exception {
        final ObjectWithMapField source = new ObjectWithMapField(map);

        // build the type
        final JsonRootGraphQLTypeBuilder typeBuilder = new JsonRootGraphQLTypeBuilder();
        final GraphQLObjectType type = (GraphQLObjectType) typeBuilder.buildType(
                ObjectWithMapField.class,
                new GraphQLTypeBuilderContext());
        final GraphQLSchema schema = GraphQLSchema.newSchema().query(type).build();

        // fetch the field value
        final GraphQLFieldDefinition field = type.getFieldDefinition("map");
        final DataFetchingEnvironment data =
                DataFetchingEnvironmentImpl.newDataFetchingEnvironment()
                .source(source)
                .fieldDefinition(field)
                .fieldType(field.getType())
                .context(mock(ExecutionContext.class))
                .build();
        return (List<Map<String, Object>>) schema.getCodeRegistry().getDataFetcher(type, field).get(data);
    }

    public static class ObjectWithMapField {
        @GraphQLName
        private Map<String, Object> map;

        public ObjectWithMapField(final Map<String, Object> map) {
            this.map = map;
        }
    }
}
