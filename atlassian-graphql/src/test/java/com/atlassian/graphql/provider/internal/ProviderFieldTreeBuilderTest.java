package com.atlassian.graphql.provider.internal;

import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.annotations.GraphQLOperationType;
import com.atlassian.graphql.annotations.GraphQLProvider;
import com.atlassian.graphql.annotations.GraphQLTypeName;
import com.atlassian.graphql.spi.GraphQLProviders;
import com.google.common.collect.Lists;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ProviderFieldTreeBuilderTest {
    @Test
    public void testBuild() {
        final ProviderFieldTreeBuilder builder = new ProviderFieldTreeBuilder();
        final TestProvider provider = new TestProvider();

        final ProviderFieldTree fieldTree = builder.build(
                Lists.newArrayList(new GraphQLProviders("", provider)),
                "Root",
                GraphQLOperationType.QUERY);
        assertProviderField(
                "root field",
                fieldTree,
                "Root",
                null,
                null);
        assertEquals(1, fieldTree.getFields().size());

        final ProviderFieldTree providerFieldTree = fieldTree.getFields().get(0);
        assertProviderField(
                "provider object field",
                providerFieldTree,
                "UsersProvider",
                null,
                null);
        assertEquals(1, providerFieldTree.getFields().size());

        final ProviderFieldTree operationField = providerFieldTree.getFields().get(0);
        assertProviderField(
                "provider method field",
                operationField,
                null,
                provider, "method");
    }

    private static void assertProviderField(
            final String message,
            final ProviderFieldTree field,
            final String expectedTypeName,
            final Object expectedProvider,
            final String expectedProviderMethodName) {

        assertEquals(message + "; typeName", expectedTypeName, field.getTypeName());
        assertEquals(message + "; provider", expectedProvider, field.getProvider());
        assertEquals(
                message + "; providerMethod",
                expectedProviderMethodName,
                field.getProviderMethod() != null ? field.getProviderMethod().getName() : null);
    }

    @GraphQLProvider
    @GraphQLName("users")
    @GraphQLTypeName("UsersProvider")
    public static class TestProvider {

        @GraphQLName("current")
        public String method() {
            return "me";
        }
    }
}
