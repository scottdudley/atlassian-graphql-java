package com.atlassian.graphql.provider;

import com.atlassian.graphql.annotations.GraphQLIDType;
import com.atlassian.graphql.annotations.GraphQLMutation;
import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.annotations.GraphQLNonNull;
import com.atlassian.graphql.annotations.GraphQLProvider;
import com.atlassian.graphql.spi.GraphQLProviders;
import com.google.common.collect.Lists;
import graphql.schema.GraphQLInputType;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLSchema;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class RootProviderGraphQLTypeBuilderTest {

    @Test
    public void testMutationInputTypes() {
        GraphQLSchema schema = testBuildSchema(new TestProviderMutationInputTypeUsedMultipleTimes(), true, true);

        GraphQLObjectType providerType = (GraphQLObjectType) schema.getMutationType().getFieldDefinitions().get(0).getType();

        GraphQLInputType simpleArgumentType = getArgumentType(providerType, "simple");
        assertEquals("String", simpleArgumentType.getName());

        GraphQLInputType simple2ArgumentType = getArgumentType(providerType, "simple2");
        assertEquals("ID", simple2ArgumentType.getName());

        GraphQLInputType enumArgumentType = getArgumentType(providerType, "enumType");
        assertEquals("EnumType", enumArgumentType.getName());

        GraphQLInputType enum2ArgumentType = getArgumentType(providerType, "enumType2");
        assertEquals("EnumType", enum2ArgumentType.getName());

        GraphQLInputType objectArgumentType = getArgumentType(providerType, "object");
        assertEquals("InputMessage", objectArgumentType.getName());

        graphql.schema.GraphQLNonNull object2ArgumentType = (graphql.schema.GraphQLNonNull)getArgumentType(providerType, "object2");
        assertEquals("InputMessage", object2ArgumentType.getWrappedType().getName());
    }

    private GraphQLInputType getArgumentType(GraphQLObjectType providerType, String name) {
        return providerType.getFieldDefinition(name).getArgument(name).getType();
    }

    @Test
    public void testBuildSchema() {
        testBuildSchema(new TestProviderQuery(), true, false);
        testBuildSchema(new TestProviderQueryAndMutation(), true, true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testProviderWithNoFields() {
        testBuildSchema(new TestProviderNoFields(), false, false);
    }

    private GraphQLSchema testBuildSchema(final Object provider, final boolean hasQueryType, final boolean hasMutationType) {
        final RootProviderGraphQLTypeBuilder providerTypeBuilder = new RootProviderGraphQLTypeBuilder();
        final List<GraphQLProviders> providers = Lists.newArrayList(new GraphQLProviders("field", provider));

        final GraphQLSchema schema = providerTypeBuilder.buildSchema("rootQuery", "rootMutation", providers, null);
        assertEquals("Has query type", hasQueryType, schema.getQueryType() != null);
        assertEquals("Has mutation type", hasMutationType, schema.getMutationType() != null);

        return schema;
    }

    public enum EnumType {
        VALUE1, VALUE2
    }

    @GraphQLProvider
    public static class TestProviderQuery {
        @GraphQLName
        public String get() {
            return "value";
        }
    }

    @GraphQLProvider
    public static class TestProviderQueryAndMutation {
        @GraphQLName
        public String get() {
            return "value";
        }

        @GraphQLName("simple")
        @GraphQLMutation
        public String simple(@GraphQLName("simple") String id) {
            return "value";
        }

        @GraphQLName("enumType")
        @GraphQLMutation
        public EnumType enumType(@GraphQLName("enumType") EnumType enumType) {
            return enumType;
        }

        @GraphQLName("object")
        @GraphQLMutation
        public String object(@GraphQLName("object") InputMessage input) {
            return "value";
        }
    }

    @GraphQLProvider
    public static class TestProviderMutationInputTypeUsedMultipleTimes extends TestProviderQueryAndMutation {

        @GraphQLName("simple2")
        @GraphQLMutation
        public String simple2(@GraphQLName("simple2") @GraphQLIDType String id) {
            return "value";
        }

        @GraphQLName("enumType2")
        @GraphQLMutation
        public EnumType enumType2(@GraphQLName("enumType2") EnumType enumType) {
            return enumType;
        }

        @GraphQLName("object2")
        @GraphQLMutation
        public String object2(@GraphQLName("object2") @GraphQLNonNull InputMessage input) {
            return "value";
        }
    }

    public static class BaseInputMessage {
        @GraphQLName
        private String inputField;

        protected BaseInputMessage() {
        }

        public BaseInputMessage(String inputField) {
            this.inputField = inputField;
        }
    }

    public static class InputMessage extends BaseInputMessage {
        public InputMessage() {
        }

        public InputMessage(String inputField) {
            super(inputField);
        }
    }

    @GraphQLProvider
    public static class TestProviderNoFields { }
}
