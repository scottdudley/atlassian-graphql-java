{
  "data": {
    "__schema": {
      "queryType": {
        "name": "Confluence"
      },
      "mutationType": null,
      "subscriptionType": null,
      "types": [
        {
          "kind": "OBJECT",
          "name": "Confluence",
          "description": null,
          "fields": [
            {
              "name": "users",
              "description": null,
              "args": [ ],
              "type": {
                "kind": "OBJECT",
                "name": "ConfluenceUsers",
                "ofType": null
              },
              "isDeprecated": false,
              "deprecationReason": null
            }
          ],
          "inputFields": null,
          "interfaces": [ ],
          "enumValues": null,
          "possibleTypes": null
        },
        {
          "kind": "OBJECT",
          "name": "ConfluenceUsers",
          "description": null,
          "fields": [
            {
              "name": "current",
              "description": null,
              "args": [ ],
              "type": {
                "kind": "INTERFACE",
                "name": "Person",
                "ofType": null
              },
              "isDeprecated": false,
              "deprecationReason": null
            }
          ],
          "inputFields": null,
          "interfaces": [ ],
          "enumValues": null,
          "possibleTypes": null
        },
        {
          "kind": "INTERFACE",
          "name": "Person",
          "description": null,
          "fields": [
            {
              "name": "type",
              "description": null,
              "args": [

              ],
              "type": {
                "kind": "SCALAR",
                "name": "String",
                "ofType": null
              },
              "isDeprecated": false,
              "deprecationReason": null
            },
            {
              "name": "displayName",
              "description": null,
              "args": [

              ],
              "type": {
                "kind": "SCALAR",
                "name": "String",
                "ofType": null
              },
              "isDeprecated": false,
              "deprecationReason": null
            }
          ],
          "inputFields": null,
          "interfaces": null,
          "enumValues": null,
          "possibleTypes": [
            {
              "kind": "OBJECT",
              "name": "User",
              "ofType": null
            }
          ]
        }
      ]
    }
  }
}