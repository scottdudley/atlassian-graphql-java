package com.atlassian.graphql.json.types;

import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.types.ObjectTypeBuilder;
import com.atlassian.graphql.types.RootGraphQLTypeBuilder;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * A builder for building GraphQLType objects from java types annotated with
 * Json (jackson), {@link XmlElement} and {@link XmlAttribute} serialization annotations.
 *
 * @see JsonRootGraphQLTypeBuilder
 */
public class JsonOrXmlRootGraphQLTypeBuilder extends JsonRootGraphQLTypeBuilder {
    private final JsonObjectTypeBuilder objectTypeBuilder;

    public JsonOrXmlRootGraphQLTypeBuilder() {
        this(null);
    }

    public JsonOrXmlRootGraphQLTypeBuilder(final GraphQLExtensions extensions) {
        super(extensions);
        objectTypeBuilder = new JsonOrXmlObjectTypeBuilder(this, extensions);
    }

    @Override
    protected ObjectTypeBuilder getObjectTypeBuilder() {
        return objectTypeBuilder;
    }
}
