package com.atlassian.graphql.utils;

import com.atlassian.graphql.annotations.GraphQLName;
import com.google.common.base.Strings;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static com.atlassian.graphql.utils.ReflectionUtils.getClazz;

/**
 * A simple deserializer for GraphQL input objects, that are usually provided from graphql-java
 * in the form of a hierarchical Map object.
 */
public class InputObjectDeserializer {
    private static Cache<Class, Map<String, Field>> fieldCache =
            CacheBuilder.newBuilder()
            .maximumSize(100)
            .build();

    @SuppressWarnings("unchecked")
    public Object deserialize(final Object value, final Type type) {
        final Class clazz = getClazz(type);
        if (value instanceof List && type instanceof ParameterizedType) {
            return deserializeList((List) value, type);
        }
        if (value instanceof Map && clazz != Map.class) {
            return deserializeObject((Map<String, Object>) value, type);
        }
        return value;
    }

    @SuppressWarnings("unchecked")
    private List deserializeList(List list, Type type) {
        return (List) list.stream()
                .map(item -> deserialize(item, ((ParameterizedType) type).getActualTypeArguments()[0]))
                .collect(Collectors.toList());
    }

    private Object deserializeObject(final Map<String, Object> map, final Type type) {
        final Class clazz = getClazz(type);
        try {
            final Object obj = clazz.getConstructor().newInstance();
            final Map<String, Field> fields = getFields(clazz);
            for (final Map.Entry<String, Object> entry : map.entrySet()) {
                final Field field = fields.get(entry.getKey());
                if (field != null) {
                    field.set(obj, deserialize(entry.getValue(), field.getGenericType()));
                }
            }
            return obj;
        } catch (ReflectiveOperationException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    private Map<String, Field> getFields(final Class type) {
        try {
            return fieldCache.get(type, () -> {
                Map<String, Field> fields = new LinkedHashMap<>();
                getFields(type, fields);
                return fields;
            });
        } catch (ExecutionException ex) {
            throw (RuntimeException) ex.getCause();
        }
    }

    private void getFields(final Class type, final Map<String, Field> fields) {
        final Class superclass = type.getSuperclass();
        if (superclass != null && superclass != Object.class) {
            getFields(superclass, fields);
        }

        for (final java.lang.reflect.Field field : type.getDeclaredFields()) {
            final GraphQLName graphqlName = AnnotationsHelper.getAnnotation(field, GraphQLName.class);
            if (graphqlName != null) {
                field.setAccessible(true);
                String fieldName = !Strings.isNullOrEmpty(graphqlName.value()) ? graphqlName.value() : field.getName();
                fields.put(fieldName, field);
            }
        }
    }
}
