package com.atlassian.graphql.utils;

import com.google.common.base.Strings;
import graphql.language.Field;
import graphql.language.FragmentDefinition;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLFieldsContainer;
import graphql.schema.GraphQLOutputType;
import graphql.schema.GraphQLType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * Evaluator for the 'expand' REST API parameter from a graphql query.
 */
public class GraphQLExpansionEvaluator {
    private final GraphQLFieldsContainer type;
    private final Map<String, FragmentDefinition> fragmentsByName;
    private final Map<String, GraphQLType> allTypes;

    public GraphQLExpansionEvaluator(
            final GraphQLOutputType type,
            final Map<String, FragmentDefinition> fragmentsByName,
            final Map<String, GraphQLType> allTypes) {

        requireNonNull(type);
        this.allTypes = requireNonNull(allTypes);
        this.fragmentsByName = requireNonNull(fragmentsByName);
        this.type = getGraphQLFieldsContainer(type);
    }

    /**
     * Evaluate the 'expand' REST API parameter from a graphql query field.
     * @param field The graphql query field that represents the output of the REST API being called
     * @param rootPath The root of path from which to evaluate the expansions
     *                 (for example the 'results' field in PageResponse)
     * @return The list of expansions
     */
    public List<String> evaluateExpansions(final Field field, final String rootPath) {
        requireNonNull(field);
        return evaluateExpansions(
                GraphQLUtils.getSubFields(fragmentsByName, field),
                type.getFieldDefinitions(),
                rootPath);
    }

    private List<String> evaluateExpansions(
            final List<Field> fields,
            final List<GraphQLFieldDefinition> fieldDefinitions,
            final String rootPath) {

        final List<String> list = new ArrayList<>();
        for (final Field field : fields) {
            final GraphQLFieldDefinition fieldDefinition = getSchemaFieldDefinitionFromQueryField(fieldDefinitions, field);
            if (fieldDefinition == null) {
                continue;
            }

            // proceed only if we're on the root path
            if (!Strings.isNullOrEmpty(rootPath) && !pathStartsWith(rootPath, field.getName())) {
                continue;
            }

            // evaluate expansions once we've navigated to the root path
            if (Strings.isNullOrEmpty(rootPath)) {
                return evaluateExpansionsFromRootPath(fields, fieldDefinitions, null);
            } else {
                // otherwise keep navigating the path
                return evaluateExpansions(
                        GraphQLUtils.getSubFields(fragmentsByName, field),
                        getSubFieldDefinitions(fieldDefinition.getType()),
                        navigate(rootPath));
            }
        }
        return list;
    }

    private List<String> evaluateExpansionsFromRootPath(
            final List<Field> fields,
            final List<GraphQLFieldDefinition> fieldDefinitions,
            final String prefix) {

        final List<String> list = new ArrayList<>();
        for (final Field field : fields) {
            final GraphQLFieldDefinition fieldDefinition = getSchemaFieldDefinitionFromQueryField(fieldDefinitions, field);
            if (fieldDefinition == null) {
                continue;
            }

            String fieldName = field.getName();
            String fieldNameOverride = GraphQLSchemaMetadata.getExpansionFieldOverride(fieldDefinition);
            if (fieldNameOverride != null) {
                fieldName = fieldNameOverride;
            }

            final String currentPath;
            if (GraphQLSchemaMetadata.isSkippedField(fieldDefinition)) {
                currentPath = !Strings.isNullOrEmpty(prefix)
                                ? prefix
                                : "";
            } else {
                currentPath = !Strings.isNullOrEmpty(prefix)
                                ? prefix + "." + fieldName
                                : fieldName;
            }
            final List<String> subExpansions = evaluateExpansionsFromRootPath(
                    GraphQLUtils.getSubFields(fragmentsByName, field),
                    getSubFieldDefinitions(fieldDefinition.getType()),
                    currentPath);

            if (subExpansions.isEmpty() && GraphQLSchemaMetadata.isExpansionField(fieldDefinition)) {
                list.add(currentPath);
            } else {
                list.addAll(subExpansions);
            }
        }
        return list;
    }

    private static boolean pathStartsWith(String path, String field) {
        return path.equals(field) || path.startsWith(field + ".");
    }

    private static String navigate(String path) {
        return path.contains(".")
               ? path.substring(path.indexOf(".") + 1)
               : "";
    }

    private static GraphQLFieldDefinition getSchemaFieldDefinitionFromQueryField(
            final List<GraphQLFieldDefinition> fieldDefinitions,
            final Field field) {

        return fieldDefinitions.stream()
               .filter(f -> f.getName().equals(field.getName()))
               .findFirst()
               .orElse(null);
    }

    private List<GraphQLFieldDefinition> getSubFieldDefinitions(GraphQLOutputType type) {
        type = (GraphQLOutputType) GraphQLUtils.unwrap(type, allTypes);
        return type instanceof GraphQLFieldsContainer
               ? ((GraphQLFieldsContainer) type).getFieldDefinitions()
               : Collections.emptyList();
    }

    private GraphQLFieldsContainer getGraphQLFieldsContainer(GraphQLOutputType type) {
        type = (GraphQLOutputType) GraphQLUtils.unwrap(type, allTypes);
        if (!(type instanceof GraphQLFieldsContainer)) {
            throw new IllegalArgumentException("Unsupported graphql type: " + type.getName());
        }
        return (GraphQLFieldsContainer) type;
    }
}
