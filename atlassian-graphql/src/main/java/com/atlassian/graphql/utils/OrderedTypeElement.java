package com.atlassian.graphql.utils;

import com.atlassian.graphql.annotations.GraphQLName;
import com.google.common.base.Strings;

import java.lang.reflect.Method;

/**
 * Represents a java type element (Field, Method, Parameter etc) that's ordered by @GraphQLName.order().
 * The default order is field/parameter layout order or method name.
 */
public class OrderedTypeElement implements Comparable<OrderedTypeElement> {
    private final Object element;
    private final String fieldName;
    private final int index;
    private final GraphQLName graphQLName;

    public OrderedTypeElement(final Object element, final String elementName, final int index, final GraphQLName graphQLName) {
        this.element = element;
        this.index = index;
        this.fieldName = graphQLName != null && !Strings.isNullOrEmpty(graphQLName.value()) ? graphQLName.value() : elementName;
        this.graphQLName = graphQLName;
    }

    public Object getElement() {
        return element;
    }

    public String getFieldName() {
        return fieldName;
    }

    public int getIndex() {
        return index;
    }

    public GraphQLName getGraphQLName() {
        return graphQLName;
    }

    private int orderIndex() {
        return graphQLName == null || graphQLName.order() == -1
               ? Integer.MAX_VALUE
               : graphQLName.order();
    }

    @Override
    public int compareTo(final OrderedTypeElement other) {
        int result = Integer.compare(orderIndex(), other.orderIndex());
        if (result == 0 && element instanceof Method && other.getElement() instanceof Method) {
            result = fieldName.compareTo(other.fieldName);
        }
        if (result == 0) {
            result = Integer.compare(index, other.getIndex());
        }
        return result;
    }
}
