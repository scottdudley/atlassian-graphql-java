package com.atlassian.graphql.spi;

import graphql.schema.GraphQLType;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Type;
import java.util.function.Function;

import static com.atlassian.graphql.utils.ReflectionUtils.getGraphqlObjectTypeName;
import static java.util.Objects.requireNonNull;

/**
 * A builder for building GraphQLType objects from java types.
 */
public interface GraphQLTypeBuilder {
    /**
     * Get the graphql type name that will be used to build the type.
     * @param type A java type, usually a Class or ParameterizedType
     * @param element The field, method or parameter
     * @param context Provides contextual information, including types built so far
     * @return The graphql type name
     */
    default String getTypeName(final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        requireNonNull(type);
        return getGraphqlObjectTypeName(type, element);
    }

    /**
     * Get whether this builder handles the given type.
     * @param type A java type, usually a Class or ParameterizedType
     * @param element The field, method or parameter
     * @return True if this builder can build the type
     */
    boolean canBuildType(final Type type, final AnnotatedElement element);

    /**
     * Build a {@link GraphQLType} object from a java type.
     * @param type A java type, usually a Class or ParameterizedType
     * @param context Provides contextual information, including types built so far
     * @return The built {@link GraphQLType}
     */
    default GraphQLType buildType(final Type type, final GraphQLTypeBuilderContext context) {
        return buildType(type, null, context);
    }

    /**
     * Build a {@link GraphQLType} object from a java type.
     * @param type A java type, usually a Class or ParameterizedType
     * @param element The field, method or parameter
     * @param context Provides contextual information, including types built so far
     * @return The built {@link GraphQLType}
     */
    default GraphQLType buildType(final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        return buildType(getTypeName(type, element, context), type, element, context);
    }

    /**
     * Build a {@link GraphQLType} object from a java type.
     * @param typeName The name to give the type
     * @param type A java type, usually a Class or ParameterizedType
     * @param context Provides contextual information, including types built so far
     * @return The built {@link GraphQLType}
     */
    default GraphQLType buildType(final String typeName, final Type type, final GraphQLTypeBuilderContext context) {
        return buildType(typeName, type, null, context);
    }

    /**
     * Build a GraphQLType object from a java type.
     * @param typeName The name to give the type
     * @param type A java type, usually a Class or ParameterizedType
     * @param element The field, method or parameter
     * @param context Provides contextual information, including types built so far
     * @return The built {@link GraphQLType}
     */
    GraphQLType buildType(final String typeName, final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context);

    /**
     * Create a value transformer for values this {@link GraphQLTypeBuilder} builds types for.
     */
    default Function<Object, Object> getValueTransformer(final Type type, final AnnotatedElement element) {
        return null;
    }
}
