package com.atlassian.graphql.spi;

import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLFieldsContainer;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Provides fields and naming for a type in a graphql field tree.
 */
public interface GraphQLTypeContributor {
    /**
     * Contribute a new name for a graphql {@link GraphQLFieldsContainer} object.
     */
    String contributeTypeName(
            final String typeName,
            final Type type,
            final GraphQLTypeBuilderContext context);

    /**
     * Contribute additional fields for a {@link GraphQLFieldsContainer} object.
     */
    void contributeFields(
            final String typeName,
            final Type type,
            final List<GraphQLFieldDefinition> fields,
            final GraphQLTypeBuilderContext context);
}
