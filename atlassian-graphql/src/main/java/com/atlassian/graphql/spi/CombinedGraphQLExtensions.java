package com.atlassian.graphql.spi;

import graphql.execution.instrumentation.ChainedInstrumentation;
import graphql.execution.instrumentation.Instrumentation;
import graphql.execution.instrumentation.SimpleInstrumentation;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLOutputType;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Member;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

/**
 * Provides an aggregation of {@link GraphQLExtensions} extension points.
 */
public class CombinedGraphQLExtensions implements GraphQLExtensions {
    private final GraphQLExtensions[] combinedExtensions;

    public CombinedGraphQLExtensions(final List<GraphQLExtensions> extensions) {
        requireNonNull(extensions);
        this.combinedExtensions = extensions.toArray(new GraphQLExtensions[extensions.size()]);
    }

    /**
     * Create an aggregation of {@link GraphQLExtensions} object, or null if extensions is null.
     */
    public static GraphQLExtensions combine(final List<GraphQLExtensions> extensions) {
        return extensions != null ? combine(extensions.toArray(new GraphQLExtensions[extensions.size()])) : null;
    }

    /**
     * Create an aggregation of {@link GraphQLExtensions} object, or null if extensions is null.
     */
    public static GraphQLExtensions combine(final GraphQLExtensions... extensions) {
        if (extensions == null) {
            return null;
        }
        if (extensions.length == 1) {
            return extensions[0];
        }

        final List<GraphQLExtensions> list = new ArrayList<>();
        for (final GraphQLExtensions item : extensions) {
            if (item == null) {
                continue;
            }
            if (item.getClass() == CombinedGraphQLExtensions.class) {
                Collections.addAll(list, ((CombinedGraphQLExtensions) item).combinedExtensions);
            } else if (item != null) {
                list.add(item);
            } else {
                throw new IllegalArgumentException(GraphQLExtensions.class.getSimpleName() + " object was null");
            }
        }
        return new CombinedGraphQLExtensions(list);
    }

    @Override
    public List<GraphQLTypeBuilder> getAdditionalTypeBuilders(
            final GraphQLTypeBuilder typeBuilder,
            final GraphQLExtensions extensions) {

        final List<GraphQLTypeBuilder> typeBuilders = new ArrayList<>();
        for (final GraphQLExtensions extension : combinedExtensions) {
            final List<GraphQLTypeBuilder> list = extension.getAdditionalTypeBuilders(typeBuilder, extensions);
            if (list != null) {
                typeBuilders.addAll(list);
            }
        }
        return typeBuilders;
    }

    @SuppressWarnings("unchecked")
    @Override
    public DataFetcher getDataFetcherThunk(final Type type, final Member accessor, DataFetcher dataFetcher) {
        for (final GraphQLExtensions extension : combinedExtensions) {
            dataFetcher = extension.getDataFetcherThunk(type, accessor, dataFetcher);
        }
        return dataFetcher;
    }

    @Override
    public DataFetcher getDataFetcher(
            final Supplier<DataFetcher> defaultDataFetcher,
            final String fieldName,
            final Member accessor,
            final Object source,
            final GraphQLOutputType responseType,
            final GraphQLTypeBuilder typeBuilder,
            final GraphQLTypeBuilderContext context,
            final GraphQLExtensions extensions) {

        DataFetcher result = null;
        for (final GraphQLExtensions extension : combinedExtensions) {
            final DataFetcher dataFetcher = extension.getDataFetcher(
                    defaultDataFetcher, fieldName, accessor, source, responseType, typeBuilder, context, extensions);
            if (dataFetcher != null) {
                if (result != null) {
                    throw new RuntimeException(
                            "More than one extension cannot return a value from GraphQLExtensions.getDataFetcher()");
                }
                result = dataFetcher;
            }
        }
        return result;
    }

    @Override
    public Function<Object, Object> getValueTransformer(final Type type, final AnnotatedElement element) {
        Function<Object, Object> result = null;
        for (final GraphQLExtensions extension : combinedExtensions) {
            final Function<Object, Object> valueTransformer = extension.getValueTransformer(type, element);
            if (valueTransformer != null) {
                if (result != null) {
                    throw new RuntimeException(
                            "More than one extension cannot return a value from GraphQLExtensions.getValueTransformer()");
                }
                result = valueTransformer;
            }
        }
        return result;
    }

    @Override
    public String contributeTypeName(
            String typeName,
            final Type type,
            final GraphQLTypeBuilderContext context) {

        String result = null;
        for (GraphQLExtensions extension : combinedExtensions) {
            final String newTypeName = extension.contributeTypeName(typeName, type, context);
            if (newTypeName != null) {
                if (result != null && !result.equals(typeName)) {
                    throw new IllegalArgumentException("Type name contributors returned inconsistent type names");
                }
                result = newTypeName;
            }
        }
        return result;
    }

    @Override
    public void contributeFields(
            final String typeName,
            final Type type,
            final List<GraphQLFieldDefinition> fields,
            final GraphQLTypeBuilderContext context) {

        for (GraphQLExtensions extension : combinedExtensions) {
            extension.contributeFields(typeName, type, fields, context);
        }
    }

    @Override
    public List<String> getExpansionRootPaths(final Type responseType) {
        List<String> list = null;
        for (GraphQLExtensions extension : combinedExtensions) {
            final List<String> rootPaths = extension.getExpansionRootPaths(responseType);
            if (rootPaths == null) {
                continue;
            }
            if (list == null) {
                list = new ArrayList<>(rootPaths);
            } else {
                for (String rootPath : rootPaths) {
                    if (!list.contains(rootPath)) {
                        list.add(rootPath);
                    }
                }
            }
        }
        return list;
    }

    @Override
    public boolean isExpansionField(final Member accessor) {
        for (GraphQLExtensions extension : combinedExtensions) {
            if (extension.isExpansionField(accessor)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Instrumentation getInstrumentation() {
        final List<Instrumentation> list = new ArrayList<>();
        for (GraphQLExtensions extension : combinedExtensions) {
            final Instrumentation instrumentation = extension.getInstrumentation();
            if (instrumentation != null && instrumentation != SimpleInstrumentation.INSTANCE) {
                list.add(instrumentation);
            }
        }
        return list.isEmpty()
               ? SimpleInstrumentation.INSTANCE
               : (list.size() == 1 ? list.get(0) : new ChainedInstrumentation(list));
    }
}
