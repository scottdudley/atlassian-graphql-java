package com.atlassian.graphql.types;

import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.spi.GraphQLExtensions;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.utils.AnnotationsHelper;
import com.atlassian.graphql.utils.ReflectionUtils;
import com.google.common.base.Throwables;
import graphql.schema.GraphQLType;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Constructor;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import static com.atlassian.graphql.utils.ReflectionUtils.getClazz;
import static java.util.Objects.requireNonNull;

/**
 * A builder for building GraphQLType objects from {@link GraphQLName} field annotated java types.
 */
public class RootGraphQLTypeBuilder implements GraphQLTypeBuilder {
    private final GraphQLTypeBuilder[] typeBuilders;
    private final GraphQLTypeBuilder[] inputTypeBuilders;
    private final ObjectTypeBuilder objectTypeBuilder;
    private final GraphQLExtensions extensions;

    public RootGraphQLTypeBuilder() {
        this(null);
    }

    public RootGraphQLTypeBuilder(final GraphQLExtensions extensions) {
        this.extensions = extensions;
        this.typeBuilders = createTypeBuilders(
                extensions != null
                ? extensions.getAdditionalTypeBuilders(this, extensions)
                : null,
                false);
        this.inputTypeBuilders = createTypeBuilders(
                extensions != null
                ? extensions.getAdditionalTypeBuilders(this, extensions)
                : null,
                true);
        this.objectTypeBuilder = new ObjectTypeBuilder(this, extensions);
    }

    protected ObjectTypeBuilder getObjectTypeBuilder() {
        return objectTypeBuilder;
    }

    private GraphQLTypeBuilder[] createTypeBuilders(final List<GraphQLTypeBuilder> additionalTypeBuilders, final boolean inputTypes) {
        final List<GraphQLTypeBuilder> typeBuilders = new ArrayList<>();
        if (additionalTypeBuilders != null) {
            typeBuilders.addAll(additionalTypeBuilders);
        }
        typeBuilders.addAll(createTypeBuilders(getRootTypeBuilder(true, inputTypes), inputTypes));
        typeBuilders.add(new InputObjectTypeBuilder(getRootTypeBuilder(true, true), extensions) {
            @Override
            public boolean canBuildType(final Type type, final AnnotatedElement element) {
                return inputTypes || element instanceof Parameter;
            }
        });
        return typeBuilders.toArray(new GraphQLTypeBuilder[0]);
    }

    protected List<GraphQLTypeBuilder> createTypeBuilders(final GraphQLTypeBuilder rootTypeBuilder, final boolean inputTypes) {
        final List<GraphQLTypeBuilder> typeBuilders = new ArrayList<>();
        typeBuilders.add(new DynamicTypeBuilder(rootTypeBuilder, extensions));
        typeBuilders.add(new ListTypeBuilder(rootTypeBuilder));
        typeBuilders.add(new MapKeyValueHierarchyTypeBuilder(rootTypeBuilder));
        typeBuilders.add(new MapTypeBuilder(rootTypeBuilder));
        typeBuilders.add(new EnumTypeBuilder());
        typeBuilders.add(new CompletableFutureTypeBuilder(rootTypeBuilder));
        typeBuilders.add(new OptionalTypeBuilder<>(rootTypeBuilder, Optional.class, optional -> optional.orElse(null)));
        typeBuilders.add(new ScalarTypeTypeBuilder());
        return typeBuilders;
    }

    private GraphQLTypeBuilder getTypeBuilder(
            final Type type,
            final AnnotatedElement element,
            final boolean evaluateNonNull,
            final boolean inputTypes) {

        return getTypeBuilder(type, element, getObjectTypeBuilder(), evaluateNonNull, inputTypes);
    }

    /**
     * Get the type builder that will be used to build a java type.
     * @param type A java type, usually a Class or ParameterizedType
     * @param element The field, method or parameter
     * @param objectTypeBuilder An GraphQLTypeBuilder that is used to build java types that themselves have fields
     * @param evaluateNonNull True to include {@link NonNullTypeBuilder}
     * @param inputTypes True to build input types only
     * @return The graphql type builder to use for the type
     */
    private GraphQLTypeBuilder getTypeBuilder(
            final Type type,
            final AnnotatedElement element,
            final GraphQLTypeBuilder objectTypeBuilder,
            final boolean evaluateNonNull,
            final boolean inputTypes) {

        requireNonNull(type);
        requireNonNull(objectTypeBuilder);

        // get the builder from the @GraphQLType annotation
        final Type graphqlType = getTypeFromAnnotation(type, element);
        if (graphqlType != null && GraphQLTypeBuilder.class.isAssignableFrom(getClazz(graphqlType))) {
            GraphQLTypeBuilder typeBuilder = instantiateGraphQLTypeBuilder(graphqlType);
            return maybeMakeNonNullable(type, element, () -> typeBuilder, evaluateNonNull).orElse(typeBuilder);
        }

        // otherwise locate the type builder from the type
        return maybeMakeNonNullable(type, element, () -> getRootTypeBuilder(false, inputTypes), evaluateNonNull)
                .orElse(findTypeBuilderByType(type, element, objectTypeBuilder, inputTypes));
    }

    @Override
    public boolean canBuildType(final Type type, final AnnotatedElement element) {
        return true;
    }

    @Override
    public String getTypeName(final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        return getTypeName(getRealType(type, element), element, context, true, false);
    }

    /**
     * Build a {@link GraphQLType} object from a java type.
     * @param type A java type, usually a Class or ParameterizedType
     * @return The built {@link GraphQLType}
     */
    public GraphQLType buildType(final Type type) {
        return buildType(type, new GraphQLTypeBuilderContext());
    }

    @Override
    public GraphQLType buildType(final String typeName, final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        return buildType(typeName, getRealType(type, element), element, context, true, false);
    }

    @Override
    public Function<Object, Object> getValueTransformer(final Type type, final AnnotatedElement element) {
        return getValueTransformer(getRealType(type, element), element, true, false);
    }

    private GraphQLTypeBuilder instantiateGraphQLTypeBuilder(Type graphqlType) {
        try {
            Constructor constructor = null;
            try {
                constructor = getClazz(graphqlType).getConstructor(GraphQLTypeBuilder.class);
            } catch (NoSuchMethodException ex) {
                //this is fine
            }
            return constructor != null
                    ? (GraphQLTypeBuilder) constructor.newInstance(this)
                    : (GraphQLTypeBuilder) getClazz(graphqlType).newInstance();
        } catch (ReflectiveOperationException ex) {
            throw Throwables.propagate(ex);
        }
    }

    private Optional<GraphQLTypeBuilder> maybeMakeNonNullable(final Type type,
                                                              final AnnotatedElement element,
                                                              final Supplier<GraphQLTypeBuilder> typeBuilder,
                                                              final boolean evaluateNonNull) {
        if (evaluateNonNull) {
            final NonNullTypeBuilder nonNullTypeBuilder = new NonNullTypeBuilder(typeBuilder.get());
            if (nonNullTypeBuilder.canBuildType(type, element)) {
                return Optional.of(nonNullTypeBuilder);
            }
        }
        return Optional.empty();
    }

    private GraphQLTypeBuilder findTypeBuilderByType(final Type type,
                                                     final AnnotatedElement element,
                                                     final GraphQLTypeBuilder objectTypeBuilder,
                                                     final boolean inputTypes) {
        for (final GraphQLTypeBuilder typeBuilder : inputTypes ? inputTypeBuilders : typeBuilders) {
            if (typeBuilder.canBuildType(type, element)) {
                return typeBuilder;
            }
        }

        return (!inputTypes && objectTypeBuilder.canBuildType(type, element))
                ? objectTypeBuilder
                : null;
    }

    private String getTypeName(
            final Type type,
            final AnnotatedElement element,
            final GraphQLTypeBuilderContext context,
            final boolean evaluateNonNull,
            final boolean inputTypes) {

        requireNonNull(type);
        return getTypeBuilder(type, element, evaluateNonNull, inputTypes).getTypeName(type, element, context);
    }

    private GraphQLType buildType(
            final String typeName,
            final Type type,
            final AnnotatedElement element,
            final GraphQLTypeBuilderContext context,
            final boolean evaluateNonNull,
            final boolean inputTypes) {

        requireNonNull(type);
        requireNonNull(context);
        return getTypeBuilder(type, element, evaluateNonNull, inputTypes).buildType(typeName, type, element, context);
    }

    private Function<Object, Object> getValueTransformer(
            final Type type,
            final AnnotatedElement element,
            boolean evaluateNonNull,
            boolean inputTypes) {

        requireNonNull(type);

        final Function<Object, Object> valueTransformer =
                getTypeBuilder(type, element, evaluateNonNull, inputTypes)
                .getValueTransformer(type, element);
        if (extensions == null) {
            return valueTransformer;
        }

        final Function<Object, Object> extensionsValueTransformer = extensions.getValueTransformer(type, element);
        if (extensionsValueTransformer != null) {
            return valueTransformer == null
                   ? extensionsValueTransformer
                   : obj -> valueTransformer.apply(extensionsValueTransformer.apply(obj));
        }
        return valueTransformer;
    }

    private GraphQLTypeBuilder getRootTypeBuilder(final boolean evaluateNonNull, final boolean inputTypes) {
        if (evaluateNonNull && !inputTypes) {
            return this;
        }
        return new GraphQLTypeBuilder() {
            @Override
            public String getTypeName(final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
                return RootGraphQLTypeBuilder.this.getTypeName(type, element, context, evaluateNonNull, inputTypes);
            }

            @Override
            public boolean canBuildType(final Type type, final AnnotatedElement element) {
                return RootGraphQLTypeBuilder.this.canBuildType(type, element);
            }

            @Override
            public GraphQLType buildType(final String typeName, final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
                return RootGraphQLTypeBuilder.this.buildType(typeName, type, element, context, evaluateNonNull, inputTypes);
            }

            @Override
            public Function<Object, Object> getValueTransformer(final Type type, final AnnotatedElement element) {
                return RootGraphQLTypeBuilder.this.getValueTransformer(type, element, evaluateNonNull, inputTypes);
            }
        };
    }

    private static Type getRealType(final Type type, final AnnotatedElement element) {
        if (element == null) {
            return type;
        }

        final Type graphqlType = getTypeFromAnnotation(type, element);
        return graphqlType != null && !GraphQLTypeBuilder.class.isAssignableFrom(getClazz(graphqlType))
               ? graphqlType
               : type;
    }

    private static Type getTypeFromAnnotation(final Type type, final AnnotatedElement element) {
        final Class clazz = getClazz(type);
        if (element != null) {
            // try the @GraphQLType from the element
            final Type fromElement = getTypeFromAnnotation(element);
            if (fromElement != null) {
                // try the @GraphQLType from the field type
                final Type fromElementType = getTypeFromAnnotation(fromElement, null);
                return fromElementType != null ? fromElementType : fromElement;
            }
        }

        // try the @GraphQLType from the type
        final Type fromType = getTypeFromAnnotation(clazz);
        if (fromType != null && !fromType.equals(type)) {
            final Type nextType = getTypeFromAnnotation(fromType, null);
            return nextType != null ? nextType : fromType;
        }
        return null;
    }

    private static Type getTypeFromAnnotation(final AnnotatedElement element) {
        final com.atlassian.graphql.annotations.GraphQLIDType graphqlIDType =
                AnnotationsHelper.getAnnotationIncludingInherited(null, element, com.atlassian.graphql.annotations.GraphQLIDType.class);
        if (graphqlIDType != null) {
            return GraphQLIDTypeBuilder.class;
        }

        final com.atlassian.graphql.annotations.GraphQLType graphqlType =
                AnnotationsHelper.getAnnotationIncludingInherited(null, element, com.atlassian.graphql.annotations.GraphQLType.class);
        return graphqlType != null
               ? getTypeFromGraphQLType(graphqlType)
               : null;
    }

    private static Type getTypeFromGraphQLType(final com.atlassian.graphql.annotations.GraphQLType graphqlType) {
        if (graphqlType.value().length == 0) {
            throw new IllegalArgumentException("@GraphQLType requires a type");
        }

        if (graphqlType.value().length == 1) {
            return graphqlType.value()[0];
        }
        return ReflectionUtils.createParameterizedType(
                graphqlType.value()[0],
                Arrays.stream(graphqlType.value()).skip(1).toArray(Class[]::new));
    }
}
