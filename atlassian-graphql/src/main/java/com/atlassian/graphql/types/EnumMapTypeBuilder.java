package com.atlassian.graphql.types;

import com.atlassian.graphql.annotations.GraphQLName;
import com.atlassian.graphql.spi.GraphQLTypeBuilder;
import com.atlassian.graphql.spi.GraphQLTypeBuilderContext;
import com.atlassian.graphql.utils.AnnotationsHelper;
import com.google.common.base.Throwables;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLOutputType;
import graphql.schema.GraphQLType;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static com.atlassian.graphql.utils.ReflectionUtils.getClazz;
import static java.util.Objects.requireNonNull;

/**
 * A GraphQLObjectType builder for Map objects where the key is an enum.
 * Sample usage:
 *
 * {@code
 *   \@GraphQLType(EnumMapTypeBuilder.class)
 *   \@GraphQLTypeName("ReadStates")
 *   Map&lt;NotificationReadState, Collection&lt;String&gt;&gt; getReadStates();
 * }
 */
public class EnumMapTypeBuilder extends MapTypeBuilder {
    private final GraphQLTypeBuilder typeBuilder;

    public EnumMapTypeBuilder(final GraphQLTypeBuilder typeBuilder) {
        super(typeBuilder);
        this.typeBuilder = typeBuilder;
    }

    @Override
    public GraphQLType buildType(final String typeName, final Type type, final AnnotatedElement element, final GraphQLTypeBuilderContext context) {
        requireNonNull(type);
        requireNonNull(context);

        final ParameterizedType parameterizedType = (ParameterizedType) type;
        final Class enumType = getClazz(parameterizedType.getActualTypeArguments()[0]);

        final GraphQLObjectType.Builder builder =
                GraphQLObjectType.newObject()
                .name(typeName);

        final GraphQLOutputType valueType = (GraphQLOutputType) typeBuilder.buildType(
                parameterizedType.getActualTypeArguments()[1], null, context);
        for (final Field field : enumType.getDeclaredFields()) {
            if (field.isEnumConstant()) {
                final GraphQLName graphQLName = AnnotationsHelper.getAnnotation(field, GraphQLName.class);
                final String name = graphQLName != null ? graphQLName.value() : field.getName();
                builder.field(
                        GraphQLFieldDefinition.newFieldDefinition()
                        .name(name)
                        .type(valueType)
                        .build());
            }
        }
        return builder.build();
    }

    @Override
    public Function<Object, Object> getValueTransformer(final Type type, final AnnotatedElement element) {
        requireNonNull(type);
        final Map<Enum, String> mapping = getMapping((Class) ((ParameterizedType) type).getActualTypeArguments()[0]);
        return obj -> mapToKeyValueList(obj, mapping);
    }

    private static Map<Enum, String> getMapping(final Class enumType) {
        Map<Enum, String> map = new HashMap<>();
        for (final Field field : enumType.getDeclaredFields()) {
            if (field.isEnumConstant()) {
                final GraphQLName graphQLName = AnnotationsHelper.getAnnotation(field, GraphQLName.class);
                try {
                    final String valueName = graphQLName != null ? graphQLName.value() : field.getName();
                    map.put((Enum) field.get(null), valueName);
                } catch (ReflectiveOperationException ex) {
                    throw Throwables.propagate(ex);
                }
            }
        }
        return map;
    }

    private static Object mapToKeyValueList(final Object obj, final Map<Enum, String> mapping) {
        if (!(obj instanceof Map)) {
            return null;
        }

        final Map<?, ?> map = (Map<?, ?>) obj;
        final Map<String, Object> list = new HashMap<>();
        for (final Map.Entry entry : map.entrySet()) {
            list.put(mapping.get(entry.getKey()), entry.getValue());
        }
        return list;
    }
}
