package com.atlassian.graphql.router;

import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Objects.requireNonNull;

public final class GraphQLRouteConfigurationBuilder<C> {
    private String queryTypeName;
    private final List<GraphQLRoute<C>> routes;

    public GraphQLRouteConfigurationBuilder() {
        queryTypeName = GraphQLRouteConfiguration.DEFAULT_NAME;
        routes = new ArrayList<>();
    }

    public GraphQLRouteConfigurationBuilder addRoute(final GraphQLRoute<C> route) {
        routes.add(route);
        return this;
    }

    public GraphQLRouteConfigurationBuilder queryTypeName(final String queryTypeName) {
        this.queryTypeName = queryTypeName;
        return this;
    }

    public GraphQLRouteConfiguration<C> build() {
        return new DefaultGraphQLRouteConfiguration<C>(queryTypeName, routes);
    }

    private static class DefaultGraphQLRouteConfiguration<C> implements GraphQLRouteConfiguration<C> {
        private final String queryTypeName;
        private final List<GraphQLRoute<C>> routes;

        private DefaultGraphQLRouteConfiguration(String queryTypeName, List<GraphQLRoute<C>> routes) {
            this.queryTypeName = requireNonNull(queryTypeName);
            this.routes = ImmutableList.copyOf(requireNonNull(routes));
        }

        public Stream<GraphQLRoute<C>> getRoutes(C context) {
            return routes.stream();
        }

        public String getQueryTypeName() {
            return queryTypeName;
        }
    }
}
