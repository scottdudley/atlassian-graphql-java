package com.atlassian.graphql.router;

import graphql.ErrorType;
import graphql.ExecutionResult;
import graphql.GraphQLError;
import graphql.language.SourceLocation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GraphQLRouterErrorFactory {

    private GraphQLRouterErrorFactory() {
    }

    /**
     * The result parameter is either an {@link ExecutionResult} or a Map that contains an "errors" entry.
     * That entry must be of the form (to express it using JSON):
     * [ { "message" : "some message" ,
     *     "locations" : [ { "line" : <integer> , "column" : <integer> } ] } ]
     * @param result
     * @return List of {@link GraphQLError}s in all cases
     */
    static List<GraphQLError> getErrors(final Object result) {
        if (result instanceof ExecutionResult) {
            return ((ExecutionResult) result).getErrors();
        } else if (result instanceof Map) {
            Object errorList = ((Map) result).get("errors");

            if (errorList == null) {
                return new ArrayList<>();
            }

            if (errorList instanceof List) {
                return buildGraphQLError((List) errorList);
            }

            throw new IllegalArgumentException();
        }
        throw new IllegalArgumentException();
    }

    private static List<GraphQLError> buildGraphQLError(List errorList) {
        List<GraphQLError> result = new ArrayList<>();

        for (Object error : errorList) {
            if (!(error instanceof Map)) {
                throw new IllegalArgumentException();
            }
            Map errorMap = (Map) error;

            result.add(new BasicGraphQLError(processMessage(errorMap), processLocations(errorMap)));
        }

        return result;
    }

    static String processMessage(Map errorMap) {
        Object errorMessage = errorMap.get("message");
        if (errorMessage != null && !(errorMessage instanceof String)) {
            throw new IllegalArgumentException();
        }

        if (errorMessage != null) {
            return (String) errorMessage;
        } else {
            // you'd assume this case would not happen, but just to cover that possibility ...
            return "";
        }
    }

    static List<SourceLocation> processLocations(Map errorMap) {
        List<SourceLocation> locations = new ArrayList<>();

        Object errorLocations = errorMap.get("locations");
        if (errorLocations == null) {
            return locations;
        }

        if (errorLocations != null && !(errorLocations instanceof List)) {
            throw new IllegalArgumentException();
        }

        try {
            for (Object mapEntry : (List) errorLocations) {
                // If either "line" or "column" is not in the map, an NPE will be thrown when null Integer is
                // auto-unboxed (to an int arg.)  Hence, catch below MUST handle NPEs!
                // seemed better than junky code to test for gets returning null ...
                locations.add(new SourceLocation((Integer) ((Map) mapEntry).get("line"),
                        (Integer) ((Map) mapEntry).get("column")));
            }
        } catch (Exception e) {
            // most likely a NumberFormatException, or perhaps a malformed map (e.g., no "line" entry)
            throw new IllegalArgumentException(e);
        }

        return locations;
    }

    static class BasicGraphQLError implements GraphQLError {
        private final String message;
        private final List<SourceLocation> locations;

        BasicGraphQLError(String message, List<SourceLocation> locations) {
            this.message = message;
            this.locations = locations;
        }

        @Override
        public String getMessage() {
            return message;
        }

        @Override
        public List<SourceLocation> getLocations() {

            return locations;
        }

        @Override
        public ErrorType getErrorType() {
            return ErrorType.DataFetchingException;
        }
    }

}
