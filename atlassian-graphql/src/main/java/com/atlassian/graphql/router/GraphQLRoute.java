package com.atlassian.graphql.router;

import com.atlassian.graphql.rest.GraphQLRestRequest;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

/**
 * A GraphQL route that may be passed to {@link GraphQLRouter}.
 */
public interface GraphQLRoute<C> {
    /**
     * Get the field and type name rules for the route.
     */
    GraphQLRouteRules getRouteRules();

    /**
     * Execute a part of a GraphQL request on the underlying graph.
     */
    CompletableFuture<Object> route(final GraphQLRestRequest request, C context) throws IOException;
}
