package com.atlassian.graphql.test.utils;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;

/**
 * Reflection helper methods that are used while binding objects to graph-ql.
 */
public class ReflectionUtils {
    /**
     * Get the {@link Class} object for a {@link Type} object.
     * This means unwrapping ParameterizedType or WildcardType objects.
     */
    public static Class getClazz(final Type type) {
        if (type instanceof ParameterizedType) {
            return (Class) ((ParameterizedType) type).getRawType();
        }
        if (type instanceof WildcardType) {
            final Type[] upperBounds = ((WildcardType) type).getUpperBounds();
            return upperBounds.length > 0 ? getClazz(upperBounds[0]) : Object.class;
        }
        return (Class) type;
    }
}
